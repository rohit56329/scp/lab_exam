const express = require('express')

const app = express()

const cors = require("cors");
const routerEmp = require("./routes/movie");
app.use(cors("*"));
app.use(express.json());
app.use("/movie", routerEmp);

app.listen(4000, '0.0.0.0', () => {
  console.log(`server started on port 4000`)
})