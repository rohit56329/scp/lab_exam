const { query } = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");

//display all data
router.get("/:movie_title",(request,response)=>{
  const movie_title=request.params;
  const connection=utils.openConnection();

  const statement= `select * from Movie where movie_title='${movie_title}''`;
  connection.query(statement, (error,result)=>{
      connection.end();
      response.send(utils.createResult(error,result));
  })

});

router.post("/add", (request, response) => {
  const { movie_title,movie_release_date,movie_time,director_name } = request.body;

  const connection = utils.openConnection();
  console.log(connection);
  const statement = `
        insert into Movie
        values
          ( default,'${movie_title}','${movie_release_date}','${movie_time}'','${director_name})'
      `;
  connection.query(statement, (error, result) => {
    connection.end();
    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:movie_title", (request, response) => {
  const { movie_title } = request.params;
  const { movie_release_date } = request.body;

  const statement = `
    update movie
    set 
    movie_release_date=${movie_release_date}
    where
    movie_title = '${movie_title}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);

    response.send(utils.createResult(error, result));
  });
});
router.delete("/remove/:name", (request, response) => {
  const { movie_title } = request.params;
  const statement = `
    delete from Movie
    where
      name = '${movie_title}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    connection.end();
    console.log(statement);
    response.send(utils.createResult(error, result));
  });
});
module.exports = router;
